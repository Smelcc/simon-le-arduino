/*************************************************
 * Config Constants
 *************************************************/
#define PIN_MUSIC D8
#define TONE_DURATION 200

/****** Number of buttons *******/
#define RED_BUTTON D6
#define BLUE_BUTTON D5
#define YELLOW_BUTTON D7

/****** Number of colors ********/
#define RED_COLOR D2
#define YELLOW_COLOR D3
#define BLUE_COLOR D1
