#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <vector>
#include <time.h>
#include "pitches.h"
#include "config.h"

// Global variables
bool isStart = false;
int speed;
std::vector<int> sequence;
int currentIndex;
const String DIFFICULTY = "5"; 
const String SPEED_DEFAULT = "1000"; 
int nbPlays = 0;
int diffMax = 0;
String curDiff = DIFFICULTY;
String curSpeed = SPEED_DEFAULT;


// http://192.168.4.1 
const char *ssid = "Simon";
const char *password = "SimonBOGOS";
ESP8266WebServer server(80);

// Structure
typedef struct _Element {
  int colorButton;
  int color;
  int note;
}Element;

// Array of struct Element
Element elements[3];

//Page Web
String baseHTML = "<!doctype html> <html> <head> <meta http-equiv='refresh' content='5; URL=http://192.168.4.1/stats'> <title>Fais péter le Simon </title> <style>body{background-color: #F5F3EB;}h1{color: blue;}p{color: red;}.center{margin: auto; width: 70%; padding: 10px;}input{float: right;}#aieaie{}#field1{height: 300px;}#field2{height: 300px;}</style> </head> <body> <br><div> <h1> The Bip-Bip Game </h1> </div><table style='width:100%'> <tr> <td style='vertical-align:top'> <fieldset style='height: 300px;' id='fieldset1'> <legend>Configuration Partie</legend> <br><br><form method='get' action='/init'> Difficultée : <input style='align:left' type='text' name='difficulty' value='[VALUEdifficulty]'/> <br><br>Vitesse : <input type='text' name='speed' value='[VALUEspeed]'/> <br><br><div style='margin: auto; width: 30%'> <button type='submit' value='Red'>Initialiser Partie</button> </div></form> <br></fieldset> </td><td style='vertical-align:top'> <fieldset style='height: 300px;' id='fieldset2'> <legend>Statistiques : </legend> <br>Nombre de Parties jouées : <span style='float: right;'>[NBPLAYS]</span> <br><br>Difficultées Max atteinte : <span style='float: right;'>[DIFFMAX]</span> <br></fieldset> </tr></table> <div style='margin: auto; width: 100%'> <fieldset> <legend>Solution</legend> <table style='width:100%'> <tr> [TABLESOLUTION] </tr></table> </fieldset> </div></body></html>";

String convertInt(int number)
{
    if (number == 0)
        return "0";
    String temp="";
    String returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}

//Méthodes des I/O
void playMusic(const int noteNumber, const int speedNote, const int noteDurations[], const int melody[]){
    for (int thisNote = 0; thisNote < 11; thisNote++) {
      int noteDuration = speedNote / noteDurations[thisNote];
      tone(PIN_MUSIC, melody[thisNote], noteDuration);
      int pauseBetweenNotes = noteDuration * 1.30;
      delay(pauseBetweenNotes);
      noTone(PIN_MUSIC);
    }
}

void playMusicVictory(){
  const int notesVictory[] = {  NOTE_ZERO, NOTE_C6 ,NOTE_C6,NOTE_C6,NOTE_C6, NOTE_GS5 , NOTE_AS5 , NOTE_C6, NOTE_ZERO , NOTE_AS5 , NOTE_C6};
  const int rythmeVictory[] = { 2,12,12,12,4,4,4,12,16,12,1 };
  const int speedNoteVictory = 1500;

  playMusic(sizeof(notesVictory)/sizeof(int), speedNoteVictory, rythmeVictory,  notesVictory);
}

void playMusicDefeat(){
  const int notesDefeat[] = { NOTE_B4, NOTE_F5,NOTE_ZERO ,NOTE_F5,NOTE_F5, NOTE_E5, NOTE_D5, NOTE_C5,NOTE_E4,NOTE_ZERO, NOTE_E4 , NOTE_C4 };
  const int noteDurations[] = { 4,4,4,4,3,3,3,4,4,4,4,4 };
  const int speedNote = 500;

  playMusic(sizeof(notesDefeat)/sizeof(int), speedNote, noteDurations,  notesDefeat);
}

void playMusicInit(){
  const int notesInit[] = { NOTE_G6, NOTE_FS6, NOTE_DS6, NOTE_A5 ,NOTE_GS5, NOTE_E6, NOTE_GS6, NOTE_C7 };
  const int noteDurations[] = { 4,4,4,4,4,4,4,2 };
  const int speedNote = 500;

  playMusic(sizeof(notesInit)/sizeof(int), speedNote, noteDurations,  notesInit);
  delay(1000);
}

int buttonListener(){
  //while (digitalRead(RED_BUTTON) == HIGH && digitalRead(BLUE_BUTTON) == HIGH && digitalRead(YELLOW_BUTTON) == HIGH ) // While pin 12 is HIGH (not activated)
    //yield(); // Do (almost) nothing -- yield to allow ESP8266 background functions
  if(digitalRead(RED_BUTTON) == LOW && digitalRead(BLUE_BUTTON) == LOW && digitalRead(YELLOW_BUTTON) == LOW)
    return -2;

  for(int i = 0; i < 3; i++){
    if(digitalRead(elements[i].colorButton) == LOW){
      if(isStart)
        tone(PIN_MUSIC, elements[i].note, TONE_DURATION);
      return elements[i].color;
    }
  }

  return -1;
}

void turnLightOn(int oneColor){
  // turn off all 
  for(int i = 0; i < 3; i++){
    digitalWrite(elements[i].color, LOW );
  }

  //we turn up what we need
  switch(oneColor){
    case RED_COLOR:
      digitalWrite(RED_COLOR, HIGH);
      Serial.println("RED_COLOR On");
    break;

    case BLUE_COLOR:
      digitalWrite(BLUE_COLOR, HIGH);
      Serial.println("BLUE_COLOR On");
    break;

    case YELLOW_COLOR:
      digitalWrite(YELLOW_COLOR, HIGH);
      Serial.println("YELLOW_COLOR On");
    break;
      
  }
}

//When we start the Game
void playSequence(){
  int timeBreak = speed * 0.10;
  int laspeed = speed * 0.90;
  Serial.println("play sequence : ");
  for(int i=0;i<sequence.size();i++){
    turnLightOn(sequence.at(i)); 
    for(int j = 0; j < 3; j++){
      if(elements[j].color == sequence.at(i))
        tone(PIN_MUSIC, elements[j].note, TONE_DURATION);
    }
    delay(laspeed);
    turnLightOn(-1);
    delay(timeBreak);
  }

}

void initialisationArray(int difficulty){
  Serial.print("initialisation Array ");
  sequence.clear();

  for(int i=0; i < difficulty; i++){
    addColorArray();
  }

  Serial.println(sequence.size());
  currentIndex = 0;
}

void upDifficulty(){
  currentIndex = 0;
  addColorArray();
}

void addColorArray(){
    int test = rand() % 3;
    if(0 == test)
      sequence.push_back(RED_COLOR);
    else if(1 == test)
      sequence.push_back(YELLOW_COLOR);
    else
      sequence.push_back(BLUE_COLOR);
}

//Méthodes de jeu
void endGame(bool sucess){
  nbPlays ++;
  if(sucess){
    //"Good Buzzer"?
    playMusicVictory();
    Serial.println("You Win!");
  }
  else{
    //"Bad Buzzer"
    playMusicDefeat();
    Serial.println("You Loose!");
  }
}
  
void play(int color){
  Serial.print("play");
  if(sequence.at(currentIndex++) == color){
    if(currentIndex > diffMax)
      diffMax = currentIndex;
      
    Serial.println("Good !");
    if(currentIndex == sequence.size()){
      playMusicVictory();
      upDifficulty();
      playSequence();
      //endGame(true);
    }
    isStart = true;
  }
    
  else{
    Serial.println("Bad !");
    endGame(false);
  }
}

//Méthodes des URLs
void handleRoot() {
  String resultatHTML = baseHTML;
  resultatHTML.replace("[VALUEdifficulty]", DIFFICULTY);
  resultatHTML.replace("[VALUEspeed]", SPEED_DEFAULT);
  resultatHTML.replace("[NBPLAYS]", "0");
  resultatHTML.replace("[DIFFMAX]", "0");
  server.send(200, "text/html", resultatHTML);
}

void initilisationGameDefault(){
  Serial.println("Initialisation Game");
  int difficulty = atoi(DIFFICULTY.c_str());
  int laspeed = atoi(SPEED_DEFAULT.c_str());
  Serial.print("difficulty = ");
  Serial.println(difficulty);
  Serial.print("speed = ");
  Serial.println(laspeed);
  initialiseElements();
  
  initialisationArray(difficulty);
  speed = laspeed;
  playMusicInit();
  playSequence();
  isStart = true;
}
void initStat() {
  String resultatHTML = baseHTML;
  resultatHTML.replace("[VALUEdifficulty]",curDiff);
  resultatHTML.replace("[VALUEspeed]",curSpeed);
  resultatHTML.replace("[DIFFMAX]",String(diffMax));
  resultatHTML.replace("[NBPLAYS]", String(nbPlays));
  resultatHTML.replace("[MESSAGE]", "Partie launch!");
  Serial.println(curDiff);
  Serial.println(curSpeed);
  String tableSolution;
  for(int i =0; i<sequence.size();i++){
      if(sequence.at(i) == BLUE_COLOR)
        tableSolution += String("<td bgcolor='blue'></td>"); 
      else if(sequence.at(i) == RED_COLOR)
        tableSolution += String("<td bgcolor='red'></td>"); 
      else if(sequence.at(i) == YELLOW_COLOR)
        tableSolution += String("<td bgcolor='yellow'></td>"); 
  }
  resultatHTML.replace("[TABLESOLUTION]",tableSolution);
  server.send(200, "text/html", resultatHTML);
}

void initilisationGame(){
  Serial.println("Initialisation Game");
  curDiff = server.arg("difficulty");
  curSpeed = server.arg("speed");
  server.send(200, "text/html", "<html><head><title>Redirection en HTML</title> <meta http-equiv='refresh' content='1; URL=http://192.168.4.1/stats'></head> <body> Loading ... </body> </html>");
  initialiseElements();
  
  initialisationArray(atoi(curDiff.c_str()));
  speed = atoi(curSpeed.c_str());
  playMusicInit();
  playSequence();
  isStart = true;
}

void initialiseElements(){
  //elements
  Element redElement;
  redElement.color = RED_COLOR;  
  redElement.colorButton = RED_BUTTON;
  redElement.note = NOTE_G6;
  elements[0] = redElement;

  Element blueElement;
  blueElement.color = BLUE_COLOR;
  blueElement.colorButton = BLUE_BUTTON;
  blueElement.note = NOTE_B6;
  elements[1] = blueElement;

  Element yellowElement;
  yellowElement.color = YELLOW_COLOR;
  yellowElement.colorButton = YELLOW_BUTTON;
  yellowElement.note = NOTE_E6;
  elements[2] = yellowElement;
}

void setupServer(){
  server.on("/", handleRoot);
  server.on("/init", initilisationGame);
  server.on("/stats", initStat);
  server.begin();
}

// the setup routine runs once when you press reset:
void setup() {
  srand((unsigned)time(NULL));
  delay(1000);
  Serial.begin(115200);
  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);

  setupServer();
  
  Serial.println("HTTP server started");
  
  //Init des LED
  pinMode(RED_COLOR, OUTPUT);
  pinMode(BLUE_COLOR, OUTPUT);
  pinMode(YELLOW_COLOR, OUTPUT);
 
  // Init des boutons
  pinMode(RED_BUTTON, INPUT_PULLUP);
  pinMode(BLUE_BUTTON, INPUT_PULLUP);
  pinMode(YELLOW_BUTTON, INPUT_PULLUP);
}


// the loop routine runs over and over again forever:
void loop() {
  server.handleClient();

  if(isStart){
    int color = buttonListener();
    if(color != -1){
      isStart = false;
      turnLightOn(color);
      play(color);
      while(color == buttonListener());
      turnLightOn(-1);
    }
  }
  else{
    if(-2 == buttonListener()){
      while(-2 == buttonListener());
      delay(500);
      initilisationGameDefault();
    }
  }
  delay(100);
} 


