simon-le-arduino

<h2>Bonjour le monde!</h2>

<b>"Simon Le Arduino"</b> est un projet IoT qui permet de construire son propre Simon, du fameux jeu "Simon says..."!<br />
Le jeu consiste en la reproduction d'une séquence de couleur en appuyant sur les boutons adéquats. <br />
Vous pourrez, pour moins de 10 euros, monter le prototype, et modifier le code présenté sur ce repo à votre convenance!<br />
<br />
Voici une <a href="https://youtu.be/Xwc5Jr1RPUo">vidéo de présentation</a><br />
<br />
Voici le détail du montage : <br />
<img src="https://gitlab.com/Smelcc/simon-le-arduino/raw/248d651905792452007649dea2d2ab92aad87249/Documentation/branchements.png"><br />
<br />
Editez le fichier <b>config.h</b> pour déterminer les ports des inputs et des outputs!<br />
<br />
Connectez vous au WiFi du Simon avec le mot de passe "SimonBOGOS" (ces valeurs sont modifiables dans le code).<br/>
Visitez ensuite <a href="http://192.168.4.1/">http://192.168.4.1/</a> pour initialiser la partie avec des paramètres customisés!<br /> 
<br />
Ici, <a href="https://gitlab.com/Smelcc/simon-le-arduino/raw/develop/Documentation/Simon%20le%20arduino.pdf">un guide utilisateur</a> <br /> 
<hr>
<br />

<h2>Hello World!</h2>
<b>"Simon Le Arduino"</b> is an IoT project that allow people making their own Simon from the famous game "Simon says..."!<br />
This game consists in reproducing a sequence of color by pushing adequates buttons. <br />
For least than $10 you can make your own prototype by hacking this repo's source code!<br />
<br />
Here is <a href="https://youtu.be/Xwc5Jr1RPUo">a presentation movie</a><br />
<br />
Here is the details of the prototype : <br />
<img src="https://gitlab.com/Smelcc/simon-le-arduino/raw/248d651905792452007649dea2d2ab92aad87249/Documentation/branchements.png"><br />
<br />
Edit <b>config.h</b> file to specify inputs and outputs!<br />
<br />
Connect to Simon WiFi with the "SimonBOGOS" password (these values can be edited in the code).<br/>
Therefore, visit <a href="http://192.168.4.1/">http://192.168.4.1/</a> to initialize a game with custom parameters!<br /> 
<br />
Ici, <a href="https://gitlab.com/Smelcc/simon-le-arduino/raw/develop/Documentation/Simon%20le%20arduino.pdf">un guide utilisateur</a> <br />